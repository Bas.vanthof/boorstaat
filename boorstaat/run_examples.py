#!/usr/bin/env python3
import matplotlib.pyplot as plt
from boorstaat.BoorStaat import create_boorstaat
create_boorstaat("boorstaat/examples/boorstaat-boring1.yaml")
plt.savefig("boorstaat/examples/boorstaat-boring1.jpg")
create_boorstaat("boorstaat/examples/boorstaat-boring3.yaml")
plt.savefig("boorstaat/examples/boorstaat-boring3.jpg")
create_boorstaat("boorstaat/examples/boorstaat-boring4.yaml")
plt.savefig("boorstaat/examples/boorstaat-boring4.jpg")
create_boorstaat("boorstaat/examples/boorstaat-boring5.yaml")
plt.savefig("boorstaat/examples/boorstaat-boring5.jpg")


