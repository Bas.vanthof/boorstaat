# Input file schrijven voor het programma BoorStaat
<div align="center">
![BoorStaat logo](boorstaat/images/program_icon.png)

_Het logo van het programma BoorStaat_
</div>

Het programma Boorstaat leest de bevindingen van een grondboring uit een invoerbestand en 
gebruikt geeft deze bevindingen grafisch weer in een boorstaat, zoals te zien in Figuur 1.

<div align="center">
![Boring 4](boorstaat/examples/boorstaat-boring4.jpg)

_Figuur 1: een voorbeeld van een boorstaat gemaakt met het programma_
</div>

De uitvoer die te zien is in Figuur 1 is gemaakt van het volgende invoerbestand:

```yaml
titel: Boring 4
grondwaterstand: 
  - 10
  - 18
bevindingen:
  - [ 1 , "Bouwvoor"    ,  "donkerbruin" ,0  , "zand"       ,  "baksteen",  "plantresten" ]
  - [ 2 , "Bouwvoor"    ,  "donkerbruin" ,0  , "zand"       ,  "baksteen",  "plantresten" ]
  - [ 3 , "Bouwvoor"    ,  "donkerbruin" ,0  , "zand"       ,               "plantresten" ]
  - [ 4 , "Bouwvoor"    ,  "donkerbruin" ,0  , "zand"       ,  "baksteen" ]
  - [ 5 , "Bouwvoor"    ,  "donkerbruin" ,0  , "zand"       ,  "baksteen" ]
  - [ 6 , "Bouwvoor"    ,  "donkerbruin" ,0  , "zand"       ,  "baksteen" ]
  - [ 7 , "Bouwvoor"    ,  "donkerbruin" ,0  , "zand"       ,  "slak",      "plantresten" ]
  - [ 8 , "Bouwvoor"    ,  "lichtbruin"  ,0  , "zand"       ,               "plantresten" ]
  - [ 9 , "Humeus"      ,  "lichtbruin"  ,0  , "kleiïg zand",  "houtskool", "plantresten" ]
  - [10 , "Humeus"      ,  "lichtbruin"  ,0  , "kleiïg zand",  "houtskool",                "roest" ]
  - [11 , "Niet humeus" ,  "bruingrijs"  ,0  , "kleiïg zand",               "plantresten", "roest" ]
  - [12 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand"       ,                              "roest" ]
  - [13 , "Niet humeus" ,  "lichtgrijs"  ,0  , "kleiïg zand",                              "roest" ]
  - [14 , "Niet humeus" ,  "lichtgrijs"  ,0  , "kleiïg zand",                              "roest" ]
  - [15 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand"       ,                              "roest" ]
  - [16 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand"       ,                              "roest" ]
  - [17 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand"       ,                              "roest" ]
  - [18 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand" ]
  - [19 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand" ]
  - [20 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand" ]
  - [21 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand" ]
  - [22 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand" ]
  - [23 , "Niet humeus" ,  "lichtgrijs"  ,0  , "zand" ]
```

Zoals te zien is in het voorbeeld, bestaat de invoer uit drie delen, `titel`, `grondwaterstand` en `bevindingen`.

+ Bij `titel` wordt een titel ingevuld, die boven de boorstaat komt te staan;
+ Bij `grondwaterstand` worden de bovenste en onderste grondwaterstanden ingevuld;  
  De gebruiikte eenheid is _decimeter onder het maaiveld_;
+ Bij `bevindingen` worden de resultaten van de boring weergegeven in een tabel.  
  Elke regel in de tabel bestaat uit _minstens_ 5 gegevens:

  1. de diepte, in _decimeter onder het maaiveld_;
  2. de ...;
  3. de kleur van de opgeboorde grond;
  4. de _korrelgrootte_ van de opgeboorde aarde;  
     Deze informatie wordt niet verwerkt door `BoorStaat`. In het bovenstaande voorbeeld was deze
     informatie ook niet beschikbaar, en daarom is steeds het antwoord `0` ingevuld.
  5. en verder: _vondsten_;

Een beperkt aantal kleuren, grondsoorten en vondsten is in `BoorStaat` ingeprogrammeerd, namelijk al 
die kleuren, grondsoorten en vondsten die in de [voorbeelden](boorstaat/examples) voorkomen.
Welke kleuren, grondsoorten en vondsten dat zijn, kan worden opgevraagd via de menu-items
`help -> Beschikbare kleuren`, 
`help -> Beschikbare grondsoorten` en
`help -> Beschikbare vondsten`.

Het zou zeer eenvoudig moeten zijn om extra
kleuren, grondsoorten en vondsten toe te voegen.

