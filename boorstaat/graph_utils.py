from matplotlib.hatch import Shapes, _hatch_types
import matplotlib.patches as mpatches
from matplotlib.patches import Rectangle
from matplotlib.legend_handler import HandlerPatch
"""
This module contains some advanced matplotlib settings 
used by BoorStaat. All these settings were found on StackOverflow.
"""

def handler_map():
    """
    Return an object that can be passed to plt.legend:
        plt.legend(handler_map=handler_map())
    it causes arrows to show up as actual arrows in the legend.
    """
    def make_legend_arrow(legend, orig_handle,
                          xdescent, ydescent,
                          width, height, fontsize):
        p = mpatches.FancyArrow(width, 0.5*height, -width, 0,
                                length_includes_head=True, head_width=0.75*height)
        return p

    return {mpatches.FancyArrow : HandlerPatch(patch_func=make_legend_arrow), }


class SquareHatch(Shapes):
    """
    This class, in combination with the initialization
       _hatch_types.append(SquareHatch)
    extends the possibilities of matplotlib-facility 'hatch' 
    with a pattern used in BoorStaat.

    Square hatch defined by a path drawn inside [-0.5, 0.5] square.
    Identifier 's'.
    """

    def __init__(self, hatch, density):
        self.filled = True
        self.size = 0.4
        self.path = Rectangle((-0.4, 0.4), 0.8, 0.4).get_path()
        self.num_rows = (hatch.count('s')) * density*2
        self.shape_vertices = self.path.vertices
        self.shape_codes = self.path.codes
        Shapes.__init__(self, hatch, density)

# Add the square hatch to the list of available hatches.
_hatch_types.append(SquareHatch)
