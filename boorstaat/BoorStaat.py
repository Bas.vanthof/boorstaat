#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import os
import yaml
from gooey import Gooey, GooeyParser
import boorstaat.graph_utils
import markdown
"""
This program constructs a user inerface (using the gooey package),
and converts input files (yaml format) into graphical representations.
"""

MAX_DIEPTE = 2.5 # [m]

# indices in table 'bevindingen' of input file
iDIEPTE, iLABEL, iCOLOR, iVONDSTEN = 0, 1, 2, 5 

DECIMETER = 0.1  # unit conversion dm -> m


def get_colors():
    """ Define some colors to be used in the input file"""
    colors = {
        'donkerbruin': (92, 64, 51),
        'geel':        (255, 255, 0),
        'lichtbruin':  (196, 164, 132),
        'lichtoranje': (242, 133, 0),
        'grijs':       (120, 120, 120),
        'lichtgrijs':  (200, 200, 200),
        'wit':         (255, 255, 255),
        'zwart':       (0, 0, 0),
    }

    def avg_color(a, b):
        return [(colors[a][i] + colors[b][i]) / 2 for i in range(3)]

    colors['lichtbruin/geel'] = avg_color('lichtbruin', 'geel')
    colors['geel/oranje'] = avg_color('lichtoranje', 'geel')
    colors['lichtgrijs/oranje'] = avg_color('lichtoranje', 'lichtgrijs')
    colors['lichtgeel/lichtoranje'] = avg_color('geel/oranje', 'wit')
    colors['lichtgeel'] = avg_color('geel', 'wit')
    colors['donkergrijs'] = avg_color('grijs', 'zwart')
    colors['donkergrijs'] = avg_color('grijs', 'donkergrijs')
    colors['lichtgeel/oranje'] = avg_color('lichtgeel', 'lichtoranje')
    colors['grijsoranje'] = avg_color('grijs', 'lichtoranje')
    colors['grijsgeel'] = avg_color('grijs', 'geel')
    colors['bruingrijs'] = avg_color('grijs', 'donkerbruin')

    for color in colors:
        colors[color] = [float(rgb)/256 for rgb in colors[color]]

    return colors


def get_grondsoorten(colors):
    """ Define the colors to represent the grondsoorten in the input file"""
    return {
        'zand':         colors['geel'],
        'leem':         colors['lichtbruin'],
        'kleiïg zand':  colors['grijsgeel'],
    }


def get_vondsten():
    """ Define the colors and the hatches (patterns) to represent the
        'vondsten' in the input file"""
    return {
        "plantresten": {"hatch": "o", "color": "green"},
        "houtskool":   {"hatch": "s", "color": "black"},
        "baksteen":    {"hatch": "s", "color": "brown"},
        "slak":        {"hatch": "*", "color": "gray"},
        "roest":       {"hatch": "*", "color": "red"}
    }


def xy_rectangle(links, rechts, hoog, laag):
    """ Return the x and y coordinates to be used in 
        plt.plot or plt.fill to draw a rectangle"""
    return [links, rechts, rechts, links, links], [hoog,  hoog, laag, laag, hoog]


def plot_colors(bevindingen):
    """ Plot the colors of the ground samples and mark them with the classification  """
    colors = get_colors()
    labels = []
    prv_diepte = 0
    for bevinding in bevindingen:
        diepte, color, label0 = bevinding[iDIEPTE], bevinding[iCOLOR], bevinding[iLABEL]
        label = label0+", "+color

        if color not in colors:
            raise RuntimeError("\n\n   Unknown color '"+color+"'\n\n")

        x, y = xy_rectangle(links=0, rechts=1,
                            hoog=prv_diepte*DECIMETER, laag=diepte*DECIMETER)
        prv_diepte = diepte
        if label in labels:
            plt.fill(x, y, color=colors[color])
        else:
            plt.fill(x, y, color=colors[color], label=label)
            labels.append(label)


def plot_grondsoorten(bevindingen):
    """ Plot the grondsoort in the appropriate color and mark them with the grondsoort-name """
    colors = get_colors()
    grondsoorten = get_grondsoorten(colors)

    x, y = xy_rectangle(links=3, rechts=4, hoog=0, laag=MAX_DIEPTE)
    plt.fill(x, y, color="white", label=' ')

    labels = []
    prv_diepte = 0
    for bevinding in bevindingen:
        diepte, grondsoort = bevinding[0], bevinding[4]
        if grondsoort in grondsoorten:
            label = grondsoort
            x, y = xy_rectangle(
                links=3, rechts=4, hoog=prv_diepte*DECIMETER, laag=diepte*DECIMETER)
            prv_diepte = diepte
            if label in labels:
                plt.fill(x, y, color=grondsoorten[grondsoort])
            else:
                plt.fill(x, y, color=grondsoorten[grondsoort], label=label)
                labels.append(label)


def plot_vondsten(bevindingen):
    """ Plot the vondsten with the appropriate hatch (pattern) and the appropriate color.
        Mark them with the name of the vondst"""
    vondst_properties = get_vondsten()
    labels = []
    prv_diepte = 0
    for bevinding in bevindingen:
        diepte, vondsten = bevinding[iDIEPTE], bevinding[iVONDSTEN:]
        n_vondsten = len(vondsten)
        for i, vondst in enumerate(vondsten):
            if vondst not in vondst_properties:
                raise RuntimeError("\n\n    Unknown vondst '"+vondst+"'\n\n")

            hatch = vondst_properties[vondst]['hatch']
            color = vondst_properties[vondst]['color']

            x, y = xy_rectangle(links=3+i/n_vondsten, rechts=3+(i+1)/n_vondsten,
                                hoog=prv_diepte*DECIMETER, laag=diepte*DECIMETER)
            if vondst in labels:
                plt.fill(x, y, color=color, linewidth=0,
                         fill=False, hatch=hatch, alpha=1)
            else:
                plt.fill(x, y, color=color, linewidth=0, fill=False,
                         hatch=hatch, alpha=1, label=vondst)
                labels.append(vondst)
        prv_diepte = diepte


def plot_grondwater(grondwaterstand):
    """ Plot a blue arrow at bovenste and at onderste grondwaterstand, 
        and add both waterstanden to the legend"""
    x, y = xy_rectangle(links=-4, rechts=-3, hoog=0, laag=MAX_DIEPTE)
    plt.fill(x, y, color='white', label=' ')

    for stand, name in zip(grondwaterstand, ['bovenste', 'onderste']):
        plt.text(1.55, DECIMETER*stand, name[0] + 'gw',
                 verticalalignment='center')
        plt.arrow(1.55, DECIMETER*stand,
                  -0.4, 0.0, color='blue', head_width=0.05,
                  label=name + ' grondwaterstand')


def lay_out():
    """ Set figure size etc to make the plot look nice"""
    plt.gcf().set_size_inches([10.0, 8.0])
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().set_frame_on(False)
    plt.ylim(0, MAX_DIEPTE)
    plt.gca().invert_yaxis()
    plt.ylabel('diepte onder maaiveld [m]')
    plt.xlim(0, 8)
    plt.legend(handler_map=boorstaat.graph_utils.handler_map())


def create_boorstaat(fname):
    """ Open the input file and create a figure to represent its contents in a sounding chart """
    with open(fname, "r") as file:
        contents = yaml.safe_load(file)
    plot_colors(contents['bevindingen'])
    plot_grondsoorten(contents['bevindingen'])
    plot_vondsten(contents['bevindingen'])
    plot_grondwater(contents['grondwaterstand'])
    plt.title(contents['titel'], fontweight='heavy')
    lay_out()


def kleuren_message():
    """ Zoek op welke kleuren in de invoerfile gebruikt mogen worden """
    kleuren = [color for color in get_colors()]
    return ("Voor de invoerfile zijn de volgende kleuren beschikbaar:\n   " +
            ', '.join(kleuren[:-1]) + " en " + kleuren[-1]
            + "\n")


def grondsoorten_message():
    """ Zoek op welke grondsoorten in de invoerfile gebruikt mogen worden """
    grondsoorten = [
        grondsoort for grondsoort in get_grondsoorten(get_colors())]
    return ("Voor de invoerfile zijn de volgende grondsoorten beschikbaar:\n   " +
            ', '.join(grondsoorten[:-1]) + " en " + grondsoorten[-1]
            + "\n")


def vondsten_message():
    """ Zoek op welke vondsten in de invoerfile gebruikt mogen worden """
    vondsten = [vondst for vondst in get_vondsten()]
    return ("Voor de invoerfile zijn de volgende vondsten beschikbaar:\n   " +
            ', '.join(vondsten[:-1]) + " en " + vondsten[-1]
            + "\n")

def menu_items():
    """
    Configure the items of the help-menu:
    + a link to the user manual
    + messages about available colors, grondsoorten and vondsten.
    """
    return [
        {'type': 'Link',
         'menuTitle': 'BoorStaat gebruikershandleiding',
         'url': 'https://gitlab.com/Bas.vanthof/boorstaat/-/blob/main/README.md'},
        {'type': 'MessageDialog',
         'menuTitle': 'Beschikbare kleuren',
         'message': kleuren_message(),
         'caption': 'Beschikbare kleuren'
         },
        {'type': 'MessageDialog',
         'menuTitle': 'Beschikbare grondsoorten',
         'message': grondsoorten_message(),
         'caption': 'Beschikbare grondsoorten'
         },
        {'type': 'MessageDialog',
         'menuTitle': 'Beschikbare vondsten',
         'message': vondsten_message(),
         'caption': 'Beschikbare vondsten'
         } ]

@Gooey(use_cmd_args=True,
       default_size=(910, 530),
       menu=[{'name': 'help', 'items': menu_items()}], 
       image_dir=os.path.join(os.path.dirname(__file__), 'images'))
def main():
    """
    Configure the GUI, run the function create_boorstaat when an input file is available,
    and print or display the result.
    """
    parser = GooeyParser(
        description="Grafische weergave van metingen van grondboringen in een boorstaat")
    parser.add_argument('-i', '--input-file', widget="FileChooser")
    parser.add_argument('-o', '--output-file', widget="FileChooser",
                        help="naam van het (optionele) uitvoerbestand.\n" +
                        "   '-' betekent: geef figuur weer op het scherm\n" +
                        "De bestandsextensie bepaald het bestandsformaat:\n" +
                        "   b.v. test.pdf wordt opgeslagen in pdf-formaat;\n" +
                        "          test.jpg in jpeg-formaat",
                        default='-')
    args = parser.parse_args()

    create_boorstaat(args.input_file)
    if args.output_file in ['-', '']:
        plt.show()
    else:
        plt.savefig(args.output_file)


if __name__ == '__main__':
    main()
