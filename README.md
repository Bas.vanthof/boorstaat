# Boorstaat

<div align="center">
![BoorStaat logo](boorstaat/images/program_icon.png)

_Het logo van het programma BoorStaat_
</div>

Het programma Boorstaat leest de bevindingen van een grondboring uit een invoerbestand en 
gebruikt geeft deze bevindingen grafisch weer in een boorstaat, zoals te zien in Figuur 1.

<div align="center">
![Boring 4](boorstaat/examples/boorstaat-boring4.jpg)

_Figuur 1: een voorbeeld van een boorstaat gemaakt met het programma_
</div>

# Installatie

Het programma wordt gedownload door het te clonen:

```bash
git clone https://gitlab.com/Bas.vanthof/boorstaat.git
cd boorstaat
```

Vervolgens wordt een _virtual environment_ gecreeerd. 
Op `linux` moet je daarvoor intypen:

```bash
python3 -m venv venv
source venv/bin/activate
pip install -e .
```

Op `Windows` is dat:

```bash
python -m venv venv
venv/scripts/activate
pip install -e .
```

## Opnieuw activeren

In elke nieuwe _command window_ moet het programma opnieuw worden geactiveerd.
Op `linux` moet je daarvoor intypen:

```bash
source venv/bin/activate
pip install -e .
```

Op `Windows` is dat:

```bash
venv/scripts/activate
pip install -e .
```

# Gebruikershandleiding
Na installatie is het programma beschikbaar. Het wordt geactiveerd door

```
Boorstaat.py
```

In het daaropvolgende scherm moeten de namen van het invoerbestand en (optioneel) het uitvoerbestand 
worden ingevuld. 

Hoe een invoerfile moet worden opgesteld, is te lezen in de [invoerbeschrijving](boorstaat/input_file.md)


