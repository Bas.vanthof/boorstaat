#!/usr/bin/env python3
import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="BoorStaat",
    version="0.0.1",
    author="Bas van 't Hof for Michiel van 't Hof",
    author_email="vanthofbas@gmail.com",
    description="BoorStaat: a tool for creating sounding charts",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Bas.vanthof/boorstaat.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.10',
    install_requires=[
      "matplotlib",
      "pyYaml",
      "gooey",
      "markdown",
    ],
    scripts = ['boorstaat/BoorStaat.py'],
)


